<?php
class Mahasiswa extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->helper('url');
    $this->load->helper('form');
    $this->load->helper('file');
    $this->load->model('M_mahasiswa');
    $this->load->model('M_hobi');
    $this->load->model('M_foto');
  }

  public function index()
  {
    $data = [];
    $mahasiswa = $this->M_mahasiswa->get_all_mahasiswa();
    foreach ($mahasiswa as $row) {
      $data[] = [
        'id' => $row['id'],
        'nim' => $row['nim'],
        'nama' => $row['nama'],
        'jenis_kelamin' => $row['jenis_kelamin'],
        'alamat' => $row['alamat'],
        'foto' => $this->db->get_where('foto', ['id_mahasiswa' => $row['id']])->result_array(),
      ];
    }
    $data['mahasiswa'] = $data;

    $this->load->view('mahasiswa/index', $data);
  }

  public function tambah()
  {
    $data['hobi'] = $this->M_hobi->get_all_hobi();
    $this->load->view('mahasiswa/tambah', $data);
  }

  public function simpan()
  {
    $this->db->trans_start();

    // Tambah Data Mahasiswa
    $this->db->insert('mahasiswa', [
      'nim' => $this->input->post('nim'),
      'nama' => $this->input->post('nama'),
      'jenis_kelamin' => $this->input->post('jenis_kelamin'),
      'alamat' => $this->input->post('alamat'),
    ]);
    $mahasiswa_id = $this->db->insert_id();

    // Tambah Data Hobi
    $hobi = [];
    foreach ($this->input->post('id_hobi[]') as $row) {
      $hobi[] = [
        'id_mahasiswa' => $mahasiswa_id,
        'id_hobi' => $row,
      ];
    }
    $this->db->insert_batch('mahasiswa_hobi', $hobi);

    // Tambah Data Foto
    $config['upload_path'] = 'public/uploads/';
    $config['allowed_types'] = 'jpg|jpeg|png';
    $config['max_size'] = 2048;

    $this->load->library('upload', $config);

    $foto = [];
    $jumlah_foto = count($_FILES['foto']['name']);
    for ($i = 0; $i < $jumlah_foto; $i++) {
      if (!empty($_FILES['foto']['name'][$i])) {

        $_FILES['file']['name'] = $_FILES['foto']['name'][$i];
        $_FILES['file']['type'] = $_FILES['foto']['type'][$i];
        $_FILES['file']['tmp_name'] = $_FILES['foto']['tmp_name'][$i];
        $_FILES['file']['error'] = $_FILES['foto']['error'][$i];
        $_FILES['file']['size'] = $_FILES['foto']['size'][$i];

        if ($this->upload->do_upload('file')) {
          $upload_data = $this->upload->data();
          $file_name = $upload_data['file_name'];
          $foto[] = [
            'id_mahasiswa' => $mahasiswa_id,
            'nama_file' => $file_name,
          ];
        } else {
          $error = array('error' => $this->upload->display_errors());

          $this->load->view('mahasiswa/tambah', $error);
        }
      }
    }

    $this->db->insert_batch('foto', $foto);
    $this->db->trans_complete();

    redirect(base_url('mahasiswa'));
  }

  public function edit($id)
  {
    $data['hobi'] = $this->M_hobi->get_all_hobi();
    $data['mahasiswa'] = $this->M_mahasiswa->get_mahasiswa_by_id($id);
    $this->load->view('mahasiswa/edit', $data);
  }

  public function update($id)
  {
    $this->db->trans_start();

    // Update Data Mahasiswa
    $this->db->where('id', $id);
    $this->db->update('mahasiswa', [
      'nim' => $this->input->post('nim'),
      'nama' => $this->input->post('nama'),
      'jenis_kelamin' => $this->input->post('jenis_kelamin'),
      'alamat' => $this->input->post('alamat'),
    ]);

    // Update Data Hobi
    if ($this->input->post('id_hobi[]')) {
      $this->db->where('id_mahasiswa', $id);
      $this->db->delete('mahasiswa_hobi');
      $hobi = [];
      foreach ($this->input->post('id_hobi[]') as $row) {
        $hobi[] = [
          'id_mahasiswa' => $id,
          'id_hobi' => $row,
        ];
      }
      $this->db->insert_batch('mahasiswa_hobi', $hobi);
    }

    $this->db->trans_complete();
    redirect(base_url('mahasiswa'));
  }

  public function hapus($id)
  {
    $this->db->trans_start();

    // Hapus Data Foto
    $this->db->select('*');
    $this->db->from('foto');
    $this->db->where('id_mahasiswa', $id);
    $query = $this->db->get();
    $result = $query->result_array();

    foreach ($result as $row) {
      unlink('public/uploads/' . $row['nama_file']);
    }

    $this->db->where('id_mahasiswa', $id);
    $this->db->delete('foto');

    // Hapus Data Hobi
    $this->db->where('id_mahasiswa', $id);
    $this->db->delete('mahasiswa_hobi');

    // Hapus Data Mahasiswa
    $this->db->where('id', $id);
    $this->db->delete('mahasiswa');
    $this->db->trans_complete();
    redirect(base_url('mahasiswa'));
  }
}
