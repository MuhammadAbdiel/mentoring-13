<?php

class M_foto extends CI_Model
{
  public function __construct()
  {
    $this->load->database();
  }

  public function get_all_foto()
  {
    $query = $this->db->get('foto');
    return $query->result_array();
  }

  public function tambah_foto($data)
  {
    return $this->db->insert('foto', $data);
  }
}
