<!DOCTYPE html>
<html>

<head>
  <title>Daftar Mahasiswa</title>
  <style>
    body {
      font-family: Arial, sans-serif;
    }

    h2 {
      background-color: #007BFF;
      color: white;
      padding: 10px;
      text-align: center;
    }

    a {
      text-decoration: none;
      color: #007BFF;
    }

    a:hover {
      text-decoration: underline;
    }

    table {
      width: 100%;
      border-collapse: collapse;
    }

    th,
    td {
      padding: 10px;
      border: 1px solid #ddd;
      text-align: left;
    }

    th {
      background-color: #007BFF;
      color: white;
    }

    tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    /* Style untuk tombol */
    .btn {
      background-color: #007BFF;
      color: white;
      padding: 5px 10px;
      border: none;
      cursor: pointer;
    }

    .btn:hover {
      background-color: #0056b3;
    }
  </style>
</head>

<body>
  <h2>Daftar Mahasiswa</h2>
  <a href="<?php echo base_url('mahasiswa/tambah'); ?>" class="btn">Tambah Mahasiswa</a>
  <table border="1">
    <tr>
      <th>ID</th>
      <th>NIM</th>
      <th>Nama</th>
      <th>Jenis Kelamin</th>
      <th>Alamat</th>
      <th>Foto</th>
      <th>Aksi</th>
    </tr>
    <?php foreach ($mahasiswa as $row) : ?>
      <tr>
        <td><?php echo $row['id']; ?></td>
        <td><?php echo $row['nim']; ?></td>
        <td><?php echo $row['nama']; ?></td>
        <td><?php echo $row['jenis_kelamin']; ?></td>
        <td><?php echo $row['alamat']; ?></td>
        <!-- foto -->
        <td>
          <?php foreach ($row['foto'] as $foto) : ?>
            <img src="<?php echo base_url('public/uploads/' . $foto['nama_file']); ?>" width="100">
          <?php endforeach; ?>
        </td>
        <td>
          <a href="<?php echo base_url('mahasiswa/edit/' . $row['id']); ?>" class="btn">Edit</a>
          <a href="<?php echo base_url('mahasiswa/hapus/' . $row['id']); ?>" class="btn">Hapus</a>
        </td>
      </tr>
    <?php endforeach; ?>
  </table>
</body>

</html>