<!DOCTYPE html>
<html>

<head>
  <title>Tambah Mahasiswa</title>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <style>
    body {
      font-family: Arial, sans-serif;
      background-color: #f4f4f4;
      margin: 0;
      padding: 0;
    }

    h2 {
      background-color: #007BFF;
      color: white;
      padding: 10px;
      text-align: center;
    }

    form {
      max-width: 400px;
      margin: 0 auto;
      padding: 20px;
      background-color: #fff;
      border: 1px solid #ddd;
      border-radius: 5px;
      box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
    }

    label {
      display: block;
      margin-bottom: 5px;
      font-weight: bold;
    }

    input[type="text"],
    input[type="file"],
    select,
    textarea {
      width: 100%;
      padding-bottom: 10px;
      margin-bottom: 10px;
      border: 1px solid #ddd;
      border-radius: 3px;
      box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.07);
    }

    select {
      height: 40px;
    }

    textarea {
      height: 100px;
    }

    input[type="submit"] {
      background-color: #007BFF;
      color: white;
      border: none;
      border-radius: 3px;
      padding: 10px 20px;
      cursor: pointer;
    }

    input[type="submit"]:hover {
      background-color: #0056b3;
    }

    .select2 {
      margin-bottom: 10px;
    }

    a {
      display: block;
      text-align: center;
      margin-top: 10px;
      color: #007BFF;
      text-decoration: none;
    }

    a:hover {
      text-decoration: underline;
    }
  </style>
</head>

<body>
  <h2>Tambah Mahasiswa</h2>
  <form method="post" action="<?php echo base_url('mahasiswa/simpan'); ?>" enctype="multipart/form-data">
    <label>NIM:</label>
    <input type="text" name="nim" required>
    <label>Nama:</label>
    <input type="text" name="nama" required>
    <label>Jenis Kelamin:</label>
    <select name="jenis_kelamin" required>
      <option value="">Pilih Jenis Kelamin</option>
      <option value="L">Laki-laki</option>
      <option value="P">Perempuan</option>
    </select>
    <label>Hobi:</label>
    <select name="id_hobi[]" class="select2" multiple required>
      <option value="">Pilih Hobi</option>
      <?php foreach ($hobi as $row) : ?>
        <option value="<?php echo $row['id']; ?>"><?php echo $row['hobi']; ?></option>
      <?php endforeach; ?>
    </select>
    <label>Alamat:</label>
    <textarea name="alamat" required></textarea>
    <label>Foto :</label>
    <input type="file" name="foto[]" multiple required>
    <input type="submit" value="Simpan">
  </form>
  <a href="<?php echo base_url('mahasiswa'); ?>">Kembali ke Daftar Mahasiswa</a>

  <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script>
    $(document).ready(function() {
      $('.select2').select2();
    });
  </script>
</body>

</html>